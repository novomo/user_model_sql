const tableBuilder = require("../../sql_table_builder/builder");
const IP = require('ip');
const upload_error = require("../../../node_error_functions/upload_error")

module.exports = async (session, db) => {
  try {
  const user_cols = {
    columns: [
      {
        label: "userID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "name", type: "VARCHAR(25)" },
      { label: "surname", type: "VARCHAR(25)" },
      { label: "email", type: "VARCHAR(60)" },
      { label: "password", type: "VARCHAR(100)" },
      { label: "accessToken", type: "VARCHAR(100)" },
      { label: "tracker", type: "VARCHAR(100)" },
      { label: "unitSize", type: "DOUBLE" },
      { label: "lastUpdated", type: "INT" },
      { label: "defaultCurrency", type: "VARCHAR(3)" },
      { label: "bankRoll", type: "INT" },
      { label: "level", type: "INT" },
      { label: "tag", type: "VARCHAR(25)" },
      { label: "discordId", type: "VARCHAR(50)" },
      { label: "googleId", type: "VARCHAR(50)" },
      { label: "facebookId", type: "VARCHAR(50)" },
    ],
    primary_key: "PRIMARY KEY (userID)",
  };

  const bank_roll_cols = {
    columns: [
      {
        label: "bankRollID",
        type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
      },
      { label: "userID", type: "INT(11) UNSIGNED" },
      { label: "incrementType", type: "VARCHAR(25)" },
      { label: "increment", type: "DOUBLE" },
      { label: "startDate", type: "INT" },
      { label: "startBank", type: "VARCHAR(100)" },
      { label: "rachet", type: "BOOLEAN" },
    ],
    primary_key: "PRIMARY KEY (bankRollID)",
    foreign_key: "FOREIGN KEY (userID) REFERENCES users (userID)",
  };

  await tableBuilder(user_cols, "in4freedom", "users", session);
  await tableBuilder(bank_roll_cols, "in4freedom", "bankrolls", session);

  /* {
      notificationSettings: {
        tokens: [],
        betting: { sports: [], bobsValue: 0.0 },
        trading: [],
        task: {
          threshold: 0.0,
          pushThreshold: 0.0,
        },
      },
      profileImage: ""
      offers: { offerTypes: [] },
      userID: userID,
    } */
  await db.createCollection("user_settings", {
    reuseExisting: true,
  });
} catch (err) {
  console.log(err)
  upload_error({
    errorTitle: "Build User DB Entities",
    machine: IP.address(),
    machineName: "API",
    errorFileName: __filename.slice(__dirname.length + 1),
    err: err,
    critical: true
  })
}

};
